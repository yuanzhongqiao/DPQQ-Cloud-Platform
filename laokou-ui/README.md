#### 安装
```shell
npm install -g pnpm
```

#### 安装
```shell
pnpm install
pnpm add uuid
pnpm add jsencrypt -S
```

#### 运行
```shell
pnpm dev
```

#### 部署
```shell
pnpm build
```
